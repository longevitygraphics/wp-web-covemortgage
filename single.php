<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

<?php

	$banner_height = get_option('lg_option_blog_single_banner_height') ? get_option('lg_option_blog_single_banner_height') : '400px';

?>
	<main>
		
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

			<div class="container py-5">
				<?php the_content(); ?>
			</div>

		<?php endwhile; endif; ?>

	</main>

<?php get_footer(); ?>