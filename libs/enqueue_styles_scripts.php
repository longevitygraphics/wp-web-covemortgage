<?php

/* BEGIN CSS FILE */

global $lg_styles, $lg_scripts;

$lg_styles = [
	(object) array(
		"handle" => "lg-fonts",
		"src" => 'https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i|Playfair+Display:400,400i,700,700i|Raleway:200,200i,300,300i,400,400i',
		"dependencies" => [],
		"version" => false
	),
	(object) array(
		"handle" => "lg-style-child",
		"src" => get_stylesheet_directory_uri() . "/assets/dist/css/main.css",
		"dependencies" => [],
		"version" => filemtime(get_stylesheet_directory() . "/assets/dist/css/main.css")
	)
];

/* END CSS FILE */

/* BEGIN JS FILE */

$lg_scripts = [
	(object) array(
		"handle" => "lg-script-child",
		"src" => get_stylesheet_directory_uri() . "/assets/dist/js/main.js",
		"dependencies" => array('jquery'),
		"version" => filemtime(get_stylesheet_directory() . "/assets/dist/js/main.js")
	)
];
/* END JS FILE */

function lg_enqueue_styles_scripts() {

	global $lg_styles, $lg_scripts;

	/* INCLUDE MAIN */

	if($lg_styles && is_array($lg_styles)){
		foreach ($lg_styles as $key => $style) {

			wp_enqueue_style(
				$style->handle,
				$style->src,
				$style->dependencies,
				$style->version
			);
		}
	}

	if($lg_scripts && is_array($lg_scripts)){
		foreach ($lg_scripts as $key => $script) {
			wp_enqueue_script(
				$script->handle,
				$script->src,
				$script->dependencies,
				$script->version
			);

			wp_enqueue_script( $script->handle );
		}
	}

	/* END INCLUDE MAIN */

}

add_action( 'wp_enqueue_scripts', 'lg_enqueue_styles_scripts' );


if($lg_styles && is_array($lg_styles)){
	foreach ($lg_styles as $key => $style) {
		add_editor_style($style->src);
	}
}

?>