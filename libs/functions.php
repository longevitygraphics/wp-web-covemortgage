<?php

	class lgFunctions{

		private static $instance = null;

		private function __construct(){
			add_action('wp_content_top', array($this, 'featured_banner_top'));
			add_action('wp_content_bottom', array($this, 'content_bottom'));
		}

		function featured_banner_top(){
			ob_start(); ?>
			<?php 
					global $post;
					if($post->post_type != "team-member"):
			?>
				
				<?php get_template_part( '/templates/template-parts/page/feature-slider' ); ?>
			<?php endif; ?> 
			<?php echo ob_get_clean();
		}

		function content_bottom(){
			ob_start(); ?>
				<?php get_template_part( '/templates/template-parts/page/content-bottom' ); ?> 
			<?php echo ob_get_clean();
		}

		public static function getInstance(){
			if (self::$instance == null)
		    {
		      self::$instance = new lgFunctions();
		    }
		 
		    return self::$instance;
		}
	}

	lgFunctions::getInstance();

?>