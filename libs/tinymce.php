<?php

	global $lg_tinymce_custom;
	
	$lg_tinymce_custom = array(
	    'title' => 'Font Family',
	    'items' =>  array(
	    	array(
				'title' => 'Raleway',
	            'selector' => 'h1, div, p, a, ul, li',
	            'classes' => 'font-raleway'
			),
			array(
				'title' => 'Open Sans',
	            'selector' => 'h1, div, p, a, ul, li',
	            'classes' => 'font-open-sans'
			),
			array(
				'title' => 'Playfair Display',
	            'selector' => 'h1, div, p, a, ul, li',
	            'classes' => 'font-playfair-display'
			)
	    )
	);

?>