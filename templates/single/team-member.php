<?php
/**
 * Main template file
 *
 */
?>

<?php get_header(); ?>

	<main>
		
		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
		<?php 
			$team_image = get_field("team_image");
			$team_name = get_field("team_name");
			$team_title = get_field("team_title");
			$team_excerpt = get_field("team_excerpt");

		?>
		<div class="container team-member d-flex flex-wrap align-items-center justify-content-center py-5">

			<div class="team-content d-flex flex-column">
				<div class="team-header d-flex flex-column col-12 pb-2">
					<h1 class="h4"><?php echo $team_name ?></h1>
					<p><?php echo $team_title ?></p>
				</div>
			
				<div class="team-body row">
					<div class="team-image col-12 col-md-3 d-flex justify-content-right align-items-center pb-4">
						<img src="<?php echo $team_image["url"]; ?>" alt="<?php echo $team_image["alt"]; ?>">
					</div>
					<div class="team-copy col-12 col-md-9">
						<p> <?php echo $team_excerpt ?> </p>
					</div>

				</div>
			</div>
		</div>

		<?php endwhile; endif; ?>

	</main>

<?php get_footer(); ?>