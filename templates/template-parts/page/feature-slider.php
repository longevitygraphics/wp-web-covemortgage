<div class="top-banner <?php if(is_front_page()){echo 'home-top-banner';} ?>" id="<?php echo sanitize_title(get_the_title()); ?>-banner">
	<?php $top_banner = get_field('top_banner', 'option'); ?>

	<?php

	if(is_tax()){
		$term = get_queried_object();
		$images = get_field('top_banner', $term);
		$text_overlay = get_field('text_overlay', $term);
	}
	elseif(is_home()){
		$images = get_field('top_banner', get_option('page_for_posts'));
		$text_overlay = get_field('text_overlay', get_option('page_for_posts'));
	}

	else{
		$images = get_field('top_banner');
		$text_overlay = get_field('text_overlay');
	}
	
	$size = 'full'; // (thumbnail, medium, large, full or custom size)

	if( $images ): ?>
	    <div class="gallery">
	        <?php foreach( $images as $image ): ?>
	            <div>
	            	<!-- <?php //echo wp_get_attachment_image( $image['ID'], $size ); ?> -->
	            	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
	            </div>
	        <?php endforeach; ?>
	    </div>
	<?php else: ?>

		<div class="default_banner">
			<?php 
				if($post->post_type == 'page'): 
				$page_default_banner = get_field('page_default_banner', 'option');
			?>
				<?php if($page_default_banner): ?>
					<img src="<?php echo $page_default_banner['url']; ?>" alt="<?php echo $page_default_banner['alt']; ?>">
				<?php endif; ?>
					
			<?php elseif($post->post_type == 'post'): 
				$blog_default_banner = get_field('blog_default_banner', 'option');
			?>

				<?php if($blog_default_banner): ?>
					<img src="<?php echo $blog_default_banner['url']; ?>" alt="<?php echo $blog_default_banner['alt']; ?>">
				<?php endif; ?>

			<?php elseif($post->post_type == 'service'): 
				$service_default_banner = get_field('service_default_banner', 'option');
			?>

				<?php if($service_default_banner): ?>
					<img src="<?php echo $service_default_banner['url']; ?>" alt="<?php echo $service_default_banner['alt']; ?>">
				<?php endif; ?>

			<?php elseif($post->post_type == 'project'): 
				$project_default_banner = get_field('project_default_banner', 'option');
			?>

				<?php if($project_default_banner): ?>
					<img src="<?php echo $project_default_banner['url']; ?>" alt="<?php echo $project_default_banner['alt']; ?>">
				<?php endif; ?>

			<?php endif; ?>
		</div>

	<?php endif; ?>


	<?php
		
		$form_active = get_field('form_active');
		$form_overlay = get_field('form_overlay', 'option');
	?>
	<div class="overlay">
		<div>
			<div>
				<?php echo $text_overlay; ?>
			</div>
		</div>
	</div>
</div>