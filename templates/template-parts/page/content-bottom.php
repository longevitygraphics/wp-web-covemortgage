
<?php if($post->post_type = 'page'): ?>
	<?php

		$footer_newsletter_subscribe_active = get_field('footer_newsletter_subscribe_active');
		$footer_newsletter_subscribe = get_field('footer_newsletter_subscribe', 'option');

	?>

	<?php if($footer_newsletter_subscribe_active == 1 && $footer_newsletter_subscribe): ?>
		<div class="py-5">
			<div class="container">
				<?php echo $footer_newsletter_subscribe ?>
			</div>
		</div>
	<?php endif; ?>


<?php endif; ?>