<?php

	global $collapse_id;
	$collapse_id = 0;
	
	switch ( get_row_layout()) {
		case 'blog':
			get_template_part('/layouts/layouts/blog');
		break;

		case 'collapse':
			get_template_part('/layouts/layouts/collapse');
		break;

		case 'faq':
			get_template_part('/layouts/layouts/faq');
		break;

		case 'team_members':
			get_template_part('/layouts/layouts/team-members');
			break;

		case 'contact':
			get_template_part('/layouts/layouts/contact');
			break;
	}

?>