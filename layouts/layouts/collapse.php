<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex collapse flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			<?php
				global $collapse_id;
				$collapse_id++;

				$collapse_section = get_sub_field('collapse_section');
			?>

			<?php if($collapse_section): ?>
				<div class="accordion" id="collapse<?php echo $collapse_id; ?>">
				<?php foreach ($collapse_section as $key => $collapse):
					$title = $collapse['title'];
					$description = $collapse['description'];
				?>
				<div class="card">
				    <div class="card-header" id="heading<?php echo $key; ?>">
				      <h5 class="mb-0">
				        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#card-collapse<?php echo $key; ?>" aria-expanded="true" aria-controls="collapse<?php echo $key; ?>">
				         <?php echo $title; ?>
				        </button>
				      </h5>
				    </div>

				    <div id="card-collapse<?php echo $key; ?>" class="collapse <?php if($key == 0): ?>show<?php endif; ?>" aria-labelledby="heading<?php echo $key; ?>" data-parent="#collapse<?php echo $collapse_id; ?>">
				      <div class="card-body">
				        <?php echo $description; ?>
				      </div>
				    </div>
				  </div>
				<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
