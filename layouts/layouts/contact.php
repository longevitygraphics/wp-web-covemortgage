<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex team-contact flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			<?php
				$member_ids = get_sub_field('contact');
			?>
			<h2 class="text-center">Have more questions?</h2>
			<p class="text-center">Contact our team</p>

			<?php if($member_ids && is_array($member_ids)): ?>
				<div class="contact-team row">
					<?php foreach ($member_ids as $key => $member_id):
						$contact = get_field('contact', $member_id);
						$title = get_field('team_name', $member_id);
					?>

					<div class="col-md-6 col-lg-4 py-3">
						<h3 class="font-italic h4 text-center"><?php echo $title; ?></h3>
						<?php echo $contact; ?>
					</div>

					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<?php wp_reset_postdata(); ?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
