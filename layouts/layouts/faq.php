<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex faq flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			<?php
				$faqs = get_sub_field('faqs');
			?>

			<h2 class="text-center mb-4">Frequently Asked Questions</h2>
			<?php if($faqs && is_array($faqs)): ?>
				<div class="row">
				<?php foreach ($faqs as $key => $faq): ?>
					<div class="col-md-6 col-lg-4 py-4">
						<h3><?php echo $faq['q']; ?></h3>
						<div><?php echo $faq['a']; ?></div>
					</div>
				<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<?php wp_reset_postdata(); ?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
