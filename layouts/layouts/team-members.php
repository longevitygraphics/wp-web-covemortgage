<?php 
/**
 * Team Members Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->
		<?php

		$post_cat = get_sub_field('team_category');
		$grid_width = get_sub_field('grid_responsive');

		$w_lg = (int)$grid_width['item_per_row_large'];
		$w_md = (int)$grid_width['item_per_row_medium'];
		$w_sm = (int)$grid_width['item_per_row_small'];
		$w_xs = (int)$grid_width['item_per_row_extra_small'];


		

		$query = new WP_Query(array ('post_type' => 'team-member', 'nopaging' => true, 'tax_query' => array(array( 'taxonomy' => 'team-category', 'field' => 'slug', 'terms' => array( $post_cat )))));
		if($query->have_posts()) {
			echo '<div class="team-members d-flex justify-content-center align-items-center container flex-wrap">';
			while ($query->have_posts() ) {

				$query->the_post();

				$team_image = get_field('team_image');
				$team_name = get_field('team_name');
				$team_title = get_field('team_title');
				$team_excerpt = get_field('team_excerpt');
				$flex_direction = (($post_cat == 'management') ? '' : 'flex-column');
				$justify_content = (($post_cat == 'management') ? 'justify-content-start' : 'justify-content-center');
				$align_content = (($post_cat == 'management') ? 'align-items-start' : 'align-items-center');

				echo '<div class="team-member d-flex col-' . $w_xs . ' col-sm-' . $w_sm . ' col-md-' . $w_md . ' col-lg-' . $w_lg . ' '.$post_cat. ' ' . $justify_content . ' ' . $align_content . ' ' . $flex_direction . ' py-4">';
				
					echo '<div class="team-image">';
						echo '
						<a href="'.get_the_permalink().'">
						<img src="'.$team_image['url'].'" alt="'.$team_image['alt'].'"></a>';
					echo '</div>';

					echo '<div class="team-content d-flex flex-column justify-content-center '. $align_content .'">';
						echo '<div class="team-name">'.$team_name.'</div>';
						echo '<div class="team-title">'.$team_title.'</div>';
						if($post_cat == 'management'){
							echo "<div class='team-description'>$team_excerpt</div>";
						}
					echo '</div>';

				echo '</div>';	
			}

		wp_reset_postdata(); 
			
			
		}
		else {
			echo "No Posts Found";
		}

		?>
</div>




<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>