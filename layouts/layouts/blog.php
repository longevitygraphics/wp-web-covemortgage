<?php 
/**
 * Text Block Layout
 *
 */
?>

<?php

	get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

	<div class="d-flex blog-category flexible_text <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
		<div class="col-12">
			<?php
				$blog_category = join(',', get_sub_field('blog_category'));
			?>
			<?php

			$posts_array = get_posts(
			    array(
			        'numberposts' => -1,
			        'category' => $blog_category
			    )
			);


			?>

			<?php if($posts_array && is_array($posts_array)): ?>
				<div class="row">
				<?php foreach ($posts_array as $key => $post):
					$id = $post->ID;
					$title = $post->post_title;
					$content = $post->post_content;
					$terms = get_the_terms($id, 'category');
					$category = '';
					$image = get_the_post_thumbnail_url();
					if($terms && is_array($terms)){
						foreach ($terms as $key => $term) {
							$category .= $term->name;
						}
					}
				?>
					<div class="col-lg-4 col-md-6">
						<div>
							<div>
								<div class="image">
									<img src="<?php echo $image; ?>" alt="">
									<div class="date"><?php echo get_the_date( 'd' ); ?><br /><?php echo get_the_date( 'M' ); ?></div>
								</div>
								<div class="description">
									<h3 class="h4 text-primary"><?php echo $category; ?></h3>
									<h2 class="h3"><?php echo $title; ?></h2>
									<div><?php echo $content; ?></div>	
								</div>
							</div>
							<div class="link">
								<a href="" class="btn btn-outline-primary">Read More</a>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>

	<?php wp_reset_postdata(); ?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	get_template_part('/layouts/partials/block-settings-end');

?>
